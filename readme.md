## Что это
Я делал тестовую работу на позицию js-dev. Задача описана в task.pdf

[ codepen demo ](http://codepen.io/mogafk/full/wMNOLM/)

\* на демо сверху картика линейки не будет отображаться, потому что изображение удалилось на хостинге
вот демо с картинкой: http://codepen.io/mogafk/full/pRZQPL
## Архитектура
4 класса:

* EventLoop
    + Кастрированный **observer**: нельзя отписывать, удалять обработчики.
* Lesson
    + Главный. Имеет одно событие "**lessonComplete**".
* Bow
    + Рисует дугу.(используется интерполяция от d3js 3 точки).
* Equation
    + Выражение чуть выше изображения линейки.
    
### Схематично отношение классов:
![Ну что поделать ссылка умерла](http://pikbox.ru/img/11/20160214183026594Untitled_Diagram.png)

### Почему я считаю, что подобная организация лучше
1. Поддерживаемость
    * Задание относительно простое. И можно было не городить кучу непонятных классов, но лапша из функций, где в каждоый вызывается другая на >2к строк кода неоспоримо будет труднее в освоении.(Другое дело, что можно выделять логические блоки, модули, комментировать хотя бы) 

2. Расширяемость
    * Допустим мы захотим изменить вид стрелочки. Пускай для вычитания мы захотим добавлять менять цвет стрелочки, направление и отображаться она должна снизу. Для этого надо создать класс например **SubBow**, который будет повторять существующий **Bow**, но иначе рисовать стрелочку и расчитывать точки. Можно пойти дальше и реализовать идею интерфейса или абстрактного класса.
    
    * Захотим сделать возможность переключения между примерами: делаем несколько экземпляров **Equation**, вешаем на какое-нибудь событие перерисовку **Lesson**.

3. Моя прелесть
    
    **..........**

### Косяки
1. Использование нативного **js** в генерации **dom**.
    
    * Если я начал использовать **d3js**, то хотя бы мог использовать его в полной мере. В итоге использовал я его лишь в одном месте
    `d3.select(this.solving).classed("hide", false);`     
    Уж так начал и хотелось единообразия.
    
2. Не зря картинка линейки называется **sprite**
    
    * очевидно ее стоило отображать по частям. Как спрайт. Допустим сначала левый кусочек, потом середину, потом правый.
    
    * У меня не получилось нормально сделать середину резиновой. Поскольку с помощью **d3** в нее заложена реальная ось, значения которой можно получить,
    то приходилось ухищиряться чтобы деление "0" можно было получить в пикселях.
    Тем не менее линейка резиновая.
    
3. Сложность, возможно быдлокодинг с притензией инжиниринг.

4. Возможно стоило использовать jquery и underscore указанные в вакансии, чтобы расположить к себе интервьвера

5. Наверно стоило использовать какое-нибудь тестирование.
    
    * но с **e2e** я опыта не имел, а писать **unit** как-то "зачем?".

### Пример использования
    > Можно найти в js/app.js
****
## Сборка
Используется **commonJS** модули. Для сборки используется **gulp-browserify**. Так же для удобства поставлен **watcher**
Установка зависимостей просто `npm install`. Потом **gulp watch**

## Конец
Я не использовал **jquery**, который указан в вакансии, но с которым мне приходиться связываться ежедневно. 

Я не имею большого опыта работы с **underscore**, но меня не пугает использование еще одного инструмента.

Просьба дать какой-нибудь фидбек.

Мне не очень хочется тратить много времени на тестовое задание, хоть я и увлекся.

Сейчас я ковыряюсь для себя с задачей подсветки регионов москвы\россии взависимости от количества маркеров на ней:
![ну мб картинка сдохла](http://pikbox.ru/img/16/20160214193005663Screenshot_from_2016_02_07_10_19_01.png)

Могу выслать подделки, которые делал для себя, связанные с веб разработкой.