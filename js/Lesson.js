var EventLoop = require("./EventLoop.js");
var Equation = require("./Equation.js");
var Bow = require("./Bow.js");

var observer = new EventLoop();

/** 
 * Создает новый блок, который будет содержать svg и линейку
 * @param {Node} container=document.body Блок, в который должен быть вложен 
 *                     весь класс
 * @param {Array.<Number>} numbers Массив складываемых элементов
 * @param {Object.<String, Number>} options опции
 *                                  options.height=640 высота
 *                                  options.width=480 ширина
 * @param {Boolean} Накладывает поверх линейки реальную ось
 * @class
 */
function Lesson(container, numbers, options, debug){
    this.options = options;
    //default values
    if(container === undefined){
        container = document.body;
    }
    if(options.width === undefined)
        this.options.width = 640;
    if(options.height === undefined)
        this.options.height = 480;
    //end default vals

    //bows
    this.bows = [];

    //create specific wrapper
    var wrapper = document.createElement("div");
    wrapper.className = "lsn-wrapper";
    wrapper.style.width = this.options.width+"px";
    wrapper.style.height = this.options.height+"px";

    var measure = document.createElement("div");
    measure.className = "lsn-wrapper_measure";

    wrapper.appendChild(measure);

    this.wrapper = container.appendChild(wrapper);

    this.equation = new Equation(this, [numbers.join("+")+"=", d3.sum(numbers)]);

    //create personal functions
    var startX = this.options.width/100*3.9,
        startY = this.options.width/100*6.7;
    this.scaleX = d3.scale.linear().range([startX, (this.options.width-startY)]).domain([0, 20]);
    this.scaleY = d3.scale.linear().range([0, (this.options.height-60)]).domain([(this.options.height-60), 0]);

    var svg = this.svg = d3.select(this.wrapper).append('svg')
        .attr('width', this.options.width)
        .attr('height', this.options.height);

    //create arrow marker for end of bows
    svg.append("defs").append("marker")
        .attr("id", "arrowhead")
        .attr("refX", 4)
        .attr("refY", 2)
        .attr("markerWidth", 15)
        .attr("markerHeight", 15)
        .attr("orient", "auto")
        .append("path")
        .attr("class", "arrow")
        .attr("d", this._line([{//hardcoded
            x: 0,y:0
        }, {
            x: 4,y:2
        },{
            x:0, y:4
        }]));



    if(debug){
        var xAxis = d3.svg.axis().scale(this.scaleX)
            .orient("bottom").ticks(4);
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (this.options.height-50) + ")")
            .call(xAxis);
    }
}
/**
 * @function используется для рисование стрелочки-маркера
 */
Lesson.prototype._line = d3.svg.line()
    .x(function(d) { return d.x; })
    .y(function(d) { return d.y; });


/**
 * @deprecated использовалась для старой реализации addBow. 
 * Сейчас дублирует Bow._bowLine
 */
Lesson.prototype._bowLine = d3.svg.line() //this - instance Lesson
    .interpolate("basis")
        /**
        * @this Lesson instance
        */
    .x(function(d) {return this.scaleX(d.x);})
    .y(function(d) { return this.scaleY(d.y); });

/**
 * @function созает класс Bow и рисует дугу
 * @param {Number} from откуда [0,20]
 * @param {Number} top куда [from,20]
 */
Lesson.prototype.addBow = function(from, to){
    var bow = new Bow(this, from, to);
    this.bows.push(bow);

    bow._render();
};
/**
 * @function создает observer и подписывается на события связанных элементов
 * запускает цикл событий
 */
Lesson.prototype.run = function(){
    var observer = this.loop = new EventLoop();
    var i=0;
    this.bows[i].showInput();

    var instance = this;
    observer.on("bowSolved", function(){
        if(instance.bows[++i] !== undefined){
            instance.bows[i].showInput();
        }else{
            instance.equation.toggleSolve();
            observer.on("equationSolved", function(){
                observer.emit("lessonComplete");
            });
        }
    });

};

module.exports = Lesson;