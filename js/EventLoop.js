function EventLoop(){
    this.events = {};
    //{
    //  eventName: [fn, fn, fn, ...]
    //}
};
EventLoop.prototype.emit = function(name){
    if(this.events[name] !== undefined){
        var fns = this.events[name];
        for(var i=0; i<fns.length; i++){
            fns[0]();
        }
    }
};
EventLoop.prototype.on = function(name, callback){
    if(this.events[name] !== undefined){
        this.events[name].push(callback);
    }else{
        this.events[name] = [];
        this.events[name].push(callback);
    }
};

module.exports = EventLoop;