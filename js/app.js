var Lesson = require("./Lesson.js");


var lesson = new Lesson(document.getElementById("dynamic"), [5,6], {
    height: 300,
    width: 800
});
lesson.addBow(0, 5);
lesson.addBow(5, 11);
lesson.run();

lesson.loop.on("lessonComplete", function(){
    alert("lesson complete");
});

