/** 
 * Создает новую дугу
 * @param {Lesson} lesson Отношение к инстансу Lesson
 * @param {Array.<String, Number>} terms [строка вида "7+4=", результ числом]
 *                                       *вобще terms, потому что рассматривалась
 *                                        возможность использования разных математических
 *                                        операторов. Для этого надо было принимать строку
 *                                        в обратной польской нотации
 *                                        или ее же, но разбитую.
 * @class
 * @classdesc используется в классе Lesson
 */
function Equation(lesson, terms){//["7 + 4 = ", 11]
    var instance = this;
    this.terms = terms;
    this.lesson = lesson;

    var numbers = document.createElement("div");
    numbers.className = "numbers-wrapper";//absolute тип модуль числа

    var computing = document.createElement("span");
    this.computing = numbers.appendChild(computing);
    this.computing.innerHTML = terms[0];

    var solvingText = document.createElement("span");
    this.solvingText = numbers.appendChild(solvingText);
    this.solvingText.innerHTML = "?";

    var solving = document.createElement("input");
    solving.className = "numbers-wrapper_solve-input hide";
    this.solving = numbers.appendChild(solving);
    this.solving.onchange = function(){
        instance.handlerSolve();
    };


    this.numbers = this.lesson.wrapper.appendChild(numbers);
};
/**
 * @function обработчик события. Вешается на onchange внутри Equation.__constructor__.
 * проверяет правильно ли вычислен пример
 */
Equation.prototype.handlerSolve = function(){
    if(this.terms[1] == this.solving.value) {
        console.log("!!решено");
        this.toggleSolve();
        this.showSolving();
        this.lesson.loop.emit("equationSolved");
    }else{
        console.log("ответ неверный");
        d3.select(this.solving).classed("lsn-wrapper_input--invalid", true);
    }
};
/**
 * @function Добавляет в результат значение
 */
Equation.prototype.showSolving = function(){
    this.solvingText.innerHTML = this.terms[1];
};
/**
 * @function переключает видимость тексстового поля для результа и 
 * самого результата
 */
Equation.prototype.toggleSolve = function(){
    if(this.solving.className.split(" ").indexOf("hide") > -1){
        d3.select(this.solving).classed("hide", false);
        this.solvingText.className = "hide";
    }else{
        d3.select(this.solving).classed("hide", true);
        this.solvingText.className = "";
    }
};

module.exports = Equation;