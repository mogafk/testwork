/** 
 * Создает новую дугу
 * @param {Lesson} relation Блок, в который должен быть вложен 
 *                     весь класс
 * @param {Number} from Массив складываемых элементов
 * @param {Number} to опции
 * @class
 * @classdesc используется внутри Lesson, Lesson содержит массива Bow в Lesson.bows
 */
function Bow(relation, from, to){
    this.lesson = relation;
    this.plots = [
        {
            x: from,
            y: 0
        },{
            x: (to-from)/2+from,
            y: (to-from)*10
        },{
            x: to,
            y: 0
        }
    ];
    this.length = to-from;
};
/**
 * @function обработчик события. Вешается на onchange внутри Bow.showInput.
 * проверяет правильно ли вычисленна длина дуги
 */
Bow.prototype.handlerSolve = function(){
    if(this.length == this.input.value) {
        this.setValue(this.length);
        this.lesson.loop.emit("bowSolved");
    }else{
        this.input.className += " lsn-wrapper_input--invalid";
    }
};
/**
 * @function убирает поле ввода и заменяет его span с цифрой
 */
Bow.prototype.setValue = function(value){
    this.lesson.wrapper.removeChild(this.input);

    var label = document.createElement("span");
    label.className = "lsn-wrapper_bow-abs";//absolute тип модуль числа

    this.label = this.lesson.wrapper.appendChild(label);
    this.label.innerHTML = value;

    var styles = window.getComputedStyle(this.label);
    var x = this.lesson.scaleX(this.plots[1].x)-parseInt(styles.width)/2,
        y = this.lesson.scaleY(this.plots[1].y)-parseInt(styles.fontSize)-3;

    this.label.style.top = y+"px";
    this.label.style.left = x+"px";
};
/**
 * @function показывает поле ввода в середине над дугой и вешает на это поле
 * обработчик Bow.hadlerSolve
 */
Bow.prototype.showInput = function(){
    var input = document.createElement("input");
    input.className = "lsn-wrapper_bow-input";

    this.input = this.lesson.wrapper.appendChild(input);

    var instance = this;
    this.input.onchange = function(){
        instance.handlerSolve();
    };

    var styles = window.getComputedStyle(this.input);
    var x = this.lesson.scaleX(this.plots[1].x)-parseInt(styles.width)/2,
        y = this.lesson.scaleY(this.plots[1].y)-parseInt(styles.height)-3;

    this.input.style.top = y+"px";
    this.input.style.left = x+"px";
};
/**
 * @function используется для рисования дуги в Bow._render
 */
Bow.prototype._bowLine = d3.svg.line()
    .interpolate("basis")
    .x(function(d) {return this.lesson.scaleX(d.x);})
    .y(function(d) { return this.lesson.scaleY(d.y); });
/**
 * @function рисует свг-элемент. Ставит ему маркер на конец.
 */
Bow.prototype._render = function(){
    this.lesson.svg.append("path")
        .attr("class", "line")
        .attr("d", this._bowLine(this.plots))
        .attr("marker-end", "url(#arrowhead)");
};

module.exports = Bow;