var gulp = require('gulp');
var browserify = require('gulp-browserify');
var watch = require('gulp-watch');
var batch = require('gulp-batch');

gulp.task('buildjs', function() {
    gulp.src('js/app.js')
        .pipe(browserify({
            insertGlobals : false,
            debug: true
        }))
        .pipe(gulp.dest('.'))
});


gulp.task('watch', function () {
    watch('js/*.js', batch(function (events, done) {
        gulp.start('buildjs', done);
    }));
});