(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/** 
 * Создает новую дугу
 * @param {Lesson} relation Блок, в который должен быть вложен 
 *                     весь класс
 * @param {Number} from Массив складываемых элементов
 * @param {Number} to опции
 * @class
 * @classdesc используется внутри Lesson, Lesson содержит массива Bow в Lesson.bows
 */
function Bow(relation, from, to){
    this.lesson = relation;
    this.plots = [
        {
            x: from,
            y: 0
        },{
            x: (to-from)/2+from,
            y: (to-from)*10
        },{
            x: to,
            y: 0
        }
    ];
    this.length = to-from;
};
/**
 * @function обработчик события. Вешается на onchange внутри Bow.showInput.
 * проверяет правильно ли вычисленна длина дуги
 */
Bow.prototype.handlerSolve = function(){
    if(this.length == this.input.value) {
        this.setValue(this.length);
        this.lesson.loop.emit("bowSolved");
    }else{
        this.input.className += " lsn-wrapper_input--invalid";
    }
};
/**
 * @function убирает поле ввода и заменяет его span с цифрой
 */
Bow.prototype.setValue = function(value){
    this.lesson.wrapper.removeChild(this.input);

    var label = document.createElement("span");
    label.className = "lsn-wrapper_bow-abs";//absolute тип модуль числа

    this.label = this.lesson.wrapper.appendChild(label);
    this.label.innerHTML = value;

    var styles = window.getComputedStyle(this.label);
    var x = this.lesson.scaleX(this.plots[1].x)-parseInt(styles.width)/2,
        y = this.lesson.scaleY(this.plots[1].y)-parseInt(styles.fontSize)-3;

    this.label.style.top = y+"px";
    this.label.style.left = x+"px";
};
/**
 * @function показывает поле ввода в середине над дугой и вешает на это поле
 * обработчик Bow.hadlerSolve
 */
Bow.prototype.showInput = function(){
    var input = document.createElement("input");
    input.className = "lsn-wrapper_bow-input";

    this.input = this.lesson.wrapper.appendChild(input);

    var instance = this;
    this.input.onchange = function(){
        instance.handlerSolve();
    };

    var styles = window.getComputedStyle(this.input);
    var x = this.lesson.scaleX(this.plots[1].x)-parseInt(styles.width)/2,
        y = this.lesson.scaleY(this.plots[1].y)-parseInt(styles.height)-3;

    this.input.style.top = y+"px";
    this.input.style.left = x+"px";
};
/**
 * @function используется для рисования дуги в Bow._render
 */
Bow.prototype._bowLine = d3.svg.line()
    .interpolate("basis")
    .x(function(d) {return this.lesson.scaleX(d.x);})
    .y(function(d) { return this.lesson.scaleY(d.y); });
/**
 * @function рисует свг-элемент. Ставит ему маркер на конец.
 */
Bow.prototype._render = function(){
    this.lesson.svg.append("path")
        .attr("class", "line")
        .attr("d", this._bowLine(this.plots))
        .attr("marker-end", "url(#arrowhead)");
};

module.exports = Bow;
},{}],2:[function(require,module,exports){
/** 
 * Создает новую дугу
 * @param {Lesson} lesson Отношение к инстансу Lesson
 * @param {Array.<String, Number>} terms [строка вида "7+4=", результ числом]
 *                                       *вобще terms, потому что рассматривалась
 *                                        возможность использования разных математических
 *                                        операторов. Для этого надо было принимать строку
 *                                        в обратной польской нотации
 *                                        или ее же, но разбитую.
 * @class
 * @classdesc используется в классе Lesson
 */
function Equation(lesson, terms){//["7 + 4 = ", 11]
    var instance = this;
    this.terms = terms;
    this.lesson = lesson;

    var numbers = document.createElement("div");
    numbers.className = "numbers-wrapper";//absolute тип модуль числа

    var computing = document.createElement("span");
    this.computing = numbers.appendChild(computing);
    this.computing.innerHTML = terms[0];

    var solvingText = document.createElement("span");
    this.solvingText = numbers.appendChild(solvingText);
    this.solvingText.innerHTML = "?";

    var solving = document.createElement("input");
    solving.className = "numbers-wrapper_solve-input hide";
    this.solving = numbers.appendChild(solving);
    this.solving.onchange = function(){
        instance.handlerSolve();
    };


    this.numbers = this.lesson.wrapper.appendChild(numbers);
};
/**
 * @function обработчик события. Вешается на onchange внутри Equation.__constructor__.
 * проверяет правильно ли вычислен пример
 */
Equation.prototype.handlerSolve = function(){
    if(this.terms[1] == this.solving.value) {
        console.log("!!решено");
        this.toggleSolve();
        this.showSolving();
        this.lesson.loop.emit("equationSolved");
    }else{
        console.log("ответ неверный");
        d3.select(this.solving).classed("lsn-wrapper_input--invalid", true);
    }
};
/**
 * @function Добавляет в результат значение
 */
Equation.prototype.showSolving = function(){
    this.solvingText.innerHTML = this.terms[1];
};
/**
 * @function переключает видимость тексстового поля для результа и 
 * самого результата
 */
Equation.prototype.toggleSolve = function(){
    if(this.solving.className.split(" ").indexOf("hide") > -1){
        d3.select(this.solving).classed("hide", false);
        this.solvingText.className = "hide";
    }else{
        d3.select(this.solving).classed("hide", true);
        this.solvingText.className = "";
    }
};

module.exports = Equation;
},{}],3:[function(require,module,exports){
function EventLoop(){
    this.events = {};
    //{
    //  eventName: [fn, fn, fn, ...]
    //}
};
EventLoop.prototype.emit = function(name){
    if(this.events[name] !== undefined){
        var fns = this.events[name];
        for(var i=0; i<fns.length; i++){
            fns[0]();
        }
    }
};
EventLoop.prototype.on = function(name, callback){
    if(this.events[name] !== undefined){
        this.events[name].push(callback);
    }else{
        this.events[name] = [];
        this.events[name].push(callback);
    }
};

module.exports = EventLoop;
},{}],4:[function(require,module,exports){
var EventLoop = require("./EventLoop.js");
var Equation = require("./Equation.js");
var Bow = require("./Bow.js");

var observer = new EventLoop();

/** 
 * Создает новый блок, который будет содержать svg и линейку
 * @param {Node} container=document.body Блок, в который должен быть вложен 
 *                     весь класс
 * @param {Array.<Number>} numbers Массив складываемых элементов
 * @param {Object.<String, Number>} options опции
 *                                  options.height=640 высота
 *                                  options.width=480 ширина
 * @param {Boolean} Накладывает поверх линейки реальную ось
 * @class
 */
function Lesson(container, numbers, options, debug){
    this.options = options;
    //default values
    if(container === undefined){
        container = document.body;
    }
    if(options.width === undefined)
        this.options.width = 640;
    if(options.height === undefined)
        this.options.height = 480;
    //end default vals

    //bows
    this.bows = [];

    //create specific wrapper
    var wrapper = document.createElement("div");
    wrapper.className = "lsn-wrapper";
    wrapper.style.width = this.options.width+"px";
    wrapper.style.height = this.options.height+"px";

    var measure = document.createElement("div");
    measure.className = "lsn-wrapper_measure";

    wrapper.appendChild(measure);

    this.wrapper = container.appendChild(wrapper);

    this.equation = new Equation(this, [numbers.join("+")+"=", d3.sum(numbers)]);

    //create personal functions
    var startX = this.options.width/100*3.9,
        startY = this.options.width/100*6.7;
    this.scaleX = d3.scale.linear().range([startX, (this.options.width-startY)]).domain([0, 20]);
    this.scaleY = d3.scale.linear().range([0, (this.options.height-60)]).domain([(this.options.height-60), 0]);

    var svg = this.svg = d3.select(this.wrapper).append('svg')
        .attr('width', this.options.width)
        .attr('height', this.options.height);

    //create arrow marker for end of bows
    svg.append("defs").append("marker")
        .attr("id", "arrowhead")
        .attr("refX", 4)
        .attr("refY", 2)
        .attr("markerWidth", 15)
        .attr("markerHeight", 15)
        .attr("orient", "auto")
        .append("path")
        .attr("class", "arrow")
        .attr("d", this._line([{//hardcoded
            x: 0,y:0
        }, {
            x: 4,y:2
        },{
            x:0, y:4
        }]));



    if(debug){
        var xAxis = d3.svg.axis().scale(this.scaleX)
            .orient("bottom").ticks(4);
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (this.options.height-50) + ")")
            .call(xAxis);
    }
}
/**
 * @function используется для рисование стрелочки-маркера
 */
Lesson.prototype._line = d3.svg.line()
    .x(function(d) { return d.x; })
    .y(function(d) { return d.y; });


/**
 * @deprecated использовалась для старой реализации addBow. 
 * Сейчас дублирует Bow._bowLine
 */
Lesson.prototype._bowLine = d3.svg.line() //this - instance Lesson
    .interpolate("basis")
        /**
        * @this Lesson instance
        */
    .x(function(d) {return this.scaleX(d.x);})
    .y(function(d) { return this.scaleY(d.y); });

/**
 * @function созает класс Bow и рисует дугу
 * @param {Number} from откуда [0,20]
 * @param {Number} top куда [from,20]
 */
Lesson.prototype.addBow = function(from, to){
    var bow = new Bow(this, from, to);
    this.bows.push(bow);

    bow._render();
};
/**
 * @function создает observer и подписывается на события связанных элементов
 * запускает цикл событий
 */
Lesson.prototype.run = function(){
    var observer = this.loop = new EventLoop();
    var i=0;
    this.bows[i].showInput();

    var instance = this;
    observer.on("bowSolved", function(){
        if(instance.bows[++i] !== undefined){
            instance.bows[i].showInput();
        }else{
            instance.equation.toggleSolve();
            observer.on("equationSolved", function(){
                observer.emit("lessonComplete");
            });
        }
    });

};

module.exports = Lesson;
},{"./Bow.js":1,"./Equation.js":2,"./EventLoop.js":3}],5:[function(require,module,exports){
var Lesson = require("./Lesson.js");


var lesson = new Lesson(document.getElementById("dynamic"), [5,6], {
    height: 300,
    width: 800
});
lesson.addBow(0, 5);
lesson.addBow(5, 11);
lesson.run();

lesson.loop.on("lessonComplete", function(){
    alert("lesson complete");
});


},{"./Lesson.js":4}]},{},[5])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL21vZ2Fmay9XZWJzdG9ybVByb2plY3RzL3VjaGkgMi9ub2RlX21vZHVsZXMvZ3VscC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvaG9tZS9tb2dhZmsvV2Vic3Rvcm1Qcm9qZWN0cy91Y2hpIDIvanMvQm93LmpzIiwiL2hvbWUvbW9nYWZrL1dlYnN0b3JtUHJvamVjdHMvdWNoaSAyL2pzL0VxdWF0aW9uLmpzIiwiL2hvbWUvbW9nYWZrL1dlYnN0b3JtUHJvamVjdHMvdWNoaSAyL2pzL0V2ZW50TG9vcC5qcyIsIi9ob21lL21vZ2Fmay9XZWJzdG9ybVByb2plY3RzL3VjaGkgMi9qcy9MZXNzb24uanMiLCIvaG9tZS9tb2dhZmsvV2Vic3Rvcm1Qcm9qZWN0cy91Y2hpIDIvanMvZmFrZV9lNmM4MzIxMy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUMvRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN6RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDNUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3Rocm93IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIil9dmFyIGY9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGYuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sZixmLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qKiBcbiAqINCh0L7Qt9C00LDQtdGCINC90L7QstGD0Y4g0LTRg9Cz0YNcbiAqIEBwYXJhbSB7TGVzc29ufSByZWxhdGlvbiDQkdC70L7Quiwg0LIg0LrQvtGC0L7RgNGL0Lkg0LTQvtC70LbQtdC9INCx0YvRgtGMINCy0LvQvtC20LXQvSBcbiAqICAgICAgICAgICAgICAgICAgICAg0LLQtdGB0Ywg0LrQu9Cw0YHRgVxuICogQHBhcmFtIHtOdW1iZXJ9IGZyb20g0JzQsNGB0YHQuNCyINGB0LrQu9Cw0LTRi9Cy0LDQtdC80YvRhSDRjdC70LXQvNC10L3RgtC+0LJcbiAqIEBwYXJhbSB7TnVtYmVyfSB0byDQvtC/0YbQuNC4XG4gKiBAY2xhc3NcbiAqIEBjbGFzc2Rlc2Mg0LjRgdC/0L7Qu9GM0LfRg9C10YLRgdGPINCy0L3Rg9GC0YDQuCBMZXNzb24sIExlc3NvbiDRgdC+0LTQtdGA0LbQuNGCINC80LDRgdGB0LjQstCwIEJvdyDQsiBMZXNzb24uYm93c1xuICovXG5mdW5jdGlvbiBCb3cocmVsYXRpb24sIGZyb20sIHRvKXtcbiAgICB0aGlzLmxlc3NvbiA9IHJlbGF0aW9uO1xuICAgIHRoaXMucGxvdHMgPSBbXG4gICAgICAgIHtcbiAgICAgICAgICAgIHg6IGZyb20sXG4gICAgICAgICAgICB5OiAwXG4gICAgICAgIH0se1xuICAgICAgICAgICAgeDogKHRvLWZyb20pLzIrZnJvbSxcbiAgICAgICAgICAgIHk6ICh0by1mcm9tKSoxMFxuICAgICAgICB9LHtcbiAgICAgICAgICAgIHg6IHRvLFxuICAgICAgICAgICAgeTogMFxuICAgICAgICB9XG4gICAgXTtcbiAgICB0aGlzLmxlbmd0aCA9IHRvLWZyb207XG59O1xuLyoqXG4gKiBAZnVuY3Rpb24g0L7QsdGA0LDQsdC+0YLRh9C40Log0YHQvtCx0YvRgtC40Y8uINCS0LXRiNCw0LXRgtGB0Y8g0L3QsCBvbmNoYW5nZSDQstC90YPRgtGA0LggQm93LnNob3dJbnB1dC5cbiAqINC/0YDQvtCy0LXRgNGP0LXRgiDQv9GA0LDQstC40LvRjNC90L4g0LvQuCDQstGL0YfQuNGB0LvQtdC90L3QsCDQtNC70LjQvdCwINC00YPQs9C4XG4gKi9cbkJvdy5wcm90b3R5cGUuaGFuZGxlclNvbHZlID0gZnVuY3Rpb24oKXtcbiAgICBpZih0aGlzLmxlbmd0aCA9PSB0aGlzLmlucHV0LnZhbHVlKSB7XG4gICAgICAgIHRoaXMuc2V0VmFsdWUodGhpcy5sZW5ndGgpO1xuICAgICAgICB0aGlzLmxlc3Nvbi5sb29wLmVtaXQoXCJib3dTb2x2ZWRcIik7XG4gICAgfWVsc2V7XG4gICAgICAgIHRoaXMuaW5wdXQuY2xhc3NOYW1lICs9IFwiIGxzbi13cmFwcGVyX2lucHV0LS1pbnZhbGlkXCI7XG4gICAgfVxufTtcbi8qKlxuICogQGZ1bmN0aW9uINGD0LHQuNGA0LDQtdGCINC/0L7Qu9C1INCy0LLQvtC00LAg0Lgg0LfQsNC80LXQvdGP0LXRgiDQtdCz0L4gc3BhbiDRgSDRhtC40YTRgNC+0LlcbiAqL1xuQm93LnByb3RvdHlwZS5zZXRWYWx1ZSA9IGZ1bmN0aW9uKHZhbHVlKXtcbiAgICB0aGlzLmxlc3Nvbi53cmFwcGVyLnJlbW92ZUNoaWxkKHRoaXMuaW5wdXQpO1xuXG4gICAgdmFyIGxhYmVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInNwYW5cIik7XG4gICAgbGFiZWwuY2xhc3NOYW1lID0gXCJsc24td3JhcHBlcl9ib3ctYWJzXCI7Ly9hYnNvbHV0ZSDRgtC40L8g0LzQvtC00YPQu9GMINGH0LjRgdC70LBcblxuICAgIHRoaXMubGFiZWwgPSB0aGlzLmxlc3Nvbi53cmFwcGVyLmFwcGVuZENoaWxkKGxhYmVsKTtcbiAgICB0aGlzLmxhYmVsLmlubmVySFRNTCA9IHZhbHVlO1xuXG4gICAgdmFyIHN0eWxlcyA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHRoaXMubGFiZWwpO1xuICAgIHZhciB4ID0gdGhpcy5sZXNzb24uc2NhbGVYKHRoaXMucGxvdHNbMV0ueCktcGFyc2VJbnQoc3R5bGVzLndpZHRoKS8yLFxuICAgICAgICB5ID0gdGhpcy5sZXNzb24uc2NhbGVZKHRoaXMucGxvdHNbMV0ueSktcGFyc2VJbnQoc3R5bGVzLmZvbnRTaXplKS0zO1xuXG4gICAgdGhpcy5sYWJlbC5zdHlsZS50b3AgPSB5K1wicHhcIjtcbiAgICB0aGlzLmxhYmVsLnN0eWxlLmxlZnQgPSB4K1wicHhcIjtcbn07XG4vKipcbiAqIEBmdW5jdGlvbiDQv9C+0LrQsNC30YvQstCw0LXRgiDQv9C+0LvQtSDQstCy0L7QtNCwINCyINGB0LXRgNC10LTQuNC90LUg0L3QsNC0INC00YPQs9C+0Lkg0Lgg0LLQtdGI0LDQtdGCINC90LAg0Y3RgtC+INC/0L7Qu9C1XG4gKiDQvtCx0YDQsNCx0L7RgtGH0LjQuiBCb3cuaGFkbGVyU29sdmVcbiAqL1xuQm93LnByb3RvdHlwZS5zaG93SW5wdXQgPSBmdW5jdGlvbigpe1xuICAgIHZhciBpbnB1dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtcbiAgICBpbnB1dC5jbGFzc05hbWUgPSBcImxzbi13cmFwcGVyX2Jvdy1pbnB1dFwiO1xuXG4gICAgdGhpcy5pbnB1dCA9IHRoaXMubGVzc29uLndyYXBwZXIuYXBwZW5kQ2hpbGQoaW5wdXQpO1xuXG4gICAgdmFyIGluc3RhbmNlID0gdGhpcztcbiAgICB0aGlzLmlucHV0Lm9uY2hhbmdlID0gZnVuY3Rpb24oKXtcbiAgICAgICAgaW5zdGFuY2UuaGFuZGxlclNvbHZlKCk7XG4gICAgfTtcblxuICAgIHZhciBzdHlsZXMgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0aGlzLmlucHV0KTtcbiAgICB2YXIgeCA9IHRoaXMubGVzc29uLnNjYWxlWCh0aGlzLnBsb3RzWzFdLngpLXBhcnNlSW50KHN0eWxlcy53aWR0aCkvMixcbiAgICAgICAgeSA9IHRoaXMubGVzc29uLnNjYWxlWSh0aGlzLnBsb3RzWzFdLnkpLXBhcnNlSW50KHN0eWxlcy5oZWlnaHQpLTM7XG5cbiAgICB0aGlzLmlucHV0LnN0eWxlLnRvcCA9IHkrXCJweFwiO1xuICAgIHRoaXMuaW5wdXQuc3R5bGUubGVmdCA9IHgrXCJweFwiO1xufTtcbi8qKlxuICogQGZ1bmN0aW9uINC40YHQv9C+0LvRjNC30YPQtdGC0YHRjyDQtNC70Y8g0YDQuNGB0L7QstCw0L3QuNGPINC00YPQs9C4INCyIEJvdy5fcmVuZGVyXG4gKi9cbkJvdy5wcm90b3R5cGUuX2Jvd0xpbmUgPSBkMy5zdmcubGluZSgpXG4gICAgLmludGVycG9sYXRlKFwiYmFzaXNcIilcbiAgICAueChmdW5jdGlvbihkKSB7cmV0dXJuIHRoaXMubGVzc29uLnNjYWxlWChkLngpO30pXG4gICAgLnkoZnVuY3Rpb24oZCkgeyByZXR1cm4gdGhpcy5sZXNzb24uc2NhbGVZKGQueSk7IH0pO1xuLyoqXG4gKiBAZnVuY3Rpb24g0YDQuNGB0YPQtdGCINGB0LLQsy3RjdC70LXQvNC10L3Rgi4g0KHRgtCw0LLQuNGCINC10LzRgyDQvNCw0YDQutC10YAg0L3QsCDQutC+0L3QtdGGLlxuICovXG5Cb3cucHJvdG90eXBlLl9yZW5kZXIgPSBmdW5jdGlvbigpe1xuICAgIHRoaXMubGVzc29uLnN2Zy5hcHBlbmQoXCJwYXRoXCIpXG4gICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJsaW5lXCIpXG4gICAgICAgIC5hdHRyKFwiZFwiLCB0aGlzLl9ib3dMaW5lKHRoaXMucGxvdHMpKVxuICAgICAgICAuYXR0cihcIm1hcmtlci1lbmRcIiwgXCJ1cmwoI2Fycm93aGVhZClcIik7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEJvdzsiLCIvKiogXG4gKiDQodC+0LfQtNCw0LXRgiDQvdC+0LLRg9GOINC00YPQs9GDXG4gKiBAcGFyYW0ge0xlc3Nvbn0gbGVzc29uINCe0YLQvdC+0YjQtdC90LjQtSDQuiDQuNC90YHRgtCw0L3RgdGDIExlc3NvblxuICogQHBhcmFtIHtBcnJheS48U3RyaW5nLCBOdW1iZXI+fSB0ZXJtcyBb0YHRgtGA0L7QutCwINCy0LjQtNCwIFwiNys0PVwiLCDRgNC10LfRg9C70YzRgiDRh9C40YHQu9C+0LxdXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICrQstC+0LHRidC1IHRlcm1zLCDQv9C+0YLQvtC80YMg0YfRgtC+INGA0LDRgdGB0LzQsNGC0YDQuNCy0LDQu9Cw0YHRjFxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0LLQvtC30LzQvtC20L3QvtGB0YLRjCDQuNGB0L/QvtC70YzQt9C+0LLQsNC90LjRjyDRgNCw0LfQvdGL0YUg0LzQsNGC0LXQvNCw0YLQuNGH0LXRgdC60LjRhVxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0L7Qv9C10YDQsNGC0L7RgNC+0LIuINCU0LvRjyDRjdGC0L7Qs9C+INC90LDQtNC+INCx0YvQu9C+INC/0YDQuNC90LjQvNCw0YLRjCDRgdGC0YDQvtC60YNcbiAqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINCyINC+0LHRgNCw0YLQvdC+0Lkg0L/QvtC70YzRgdC60L7QuSDQvdC+0YLQsNGG0LjQuFxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0LjQu9C4INC10LUg0LbQtSwg0L3QviDRgNCw0LfQsdC40YLRg9GOLlxuICogQGNsYXNzXG4gKiBAY2xhc3NkZXNjINC40YHQv9C+0LvRjNC30YPQtdGC0YHRjyDQsiDQutC70LDRgdGB0LUgTGVzc29uXG4gKi9cbmZ1bmN0aW9uIEVxdWF0aW9uKGxlc3NvbiwgdGVybXMpey8vW1wiNyArIDQgPSBcIiwgMTFdXG4gICAgdmFyIGluc3RhbmNlID0gdGhpcztcbiAgICB0aGlzLnRlcm1zID0gdGVybXM7XG4gICAgdGhpcy5sZXNzb24gPSBsZXNzb247XG5cbiAgICB2YXIgbnVtYmVycyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgbnVtYmVycy5jbGFzc05hbWUgPSBcIm51bWJlcnMtd3JhcHBlclwiOy8vYWJzb2x1dGUg0YLQuNC/INC80L7QtNGD0LvRjCDRh9C40YHQu9CwXG5cbiAgICB2YXIgY29tcHV0aW5nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInNwYW5cIik7XG4gICAgdGhpcy5jb21wdXRpbmcgPSBudW1iZXJzLmFwcGVuZENoaWxkKGNvbXB1dGluZyk7XG4gICAgdGhpcy5jb21wdXRpbmcuaW5uZXJIVE1MID0gdGVybXNbMF07XG5cbiAgICB2YXIgc29sdmluZ1RleHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3BhblwiKTtcbiAgICB0aGlzLnNvbHZpbmdUZXh0ID0gbnVtYmVycy5hcHBlbmRDaGlsZChzb2x2aW5nVGV4dCk7XG4gICAgdGhpcy5zb2x2aW5nVGV4dC5pbm5lckhUTUwgPSBcIj9cIjtcblxuICAgIHZhciBzb2x2aW5nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImlucHV0XCIpO1xuICAgIHNvbHZpbmcuY2xhc3NOYW1lID0gXCJudW1iZXJzLXdyYXBwZXJfc29sdmUtaW5wdXQgaGlkZVwiO1xuICAgIHRoaXMuc29sdmluZyA9IG51bWJlcnMuYXBwZW5kQ2hpbGQoc29sdmluZyk7XG4gICAgdGhpcy5zb2x2aW5nLm9uY2hhbmdlID0gZnVuY3Rpb24oKXtcbiAgICAgICAgaW5zdGFuY2UuaGFuZGxlclNvbHZlKCk7XG4gICAgfTtcblxuXG4gICAgdGhpcy5udW1iZXJzID0gdGhpcy5sZXNzb24ud3JhcHBlci5hcHBlbmRDaGlsZChudW1iZXJzKTtcbn07XG4vKipcbiAqIEBmdW5jdGlvbiDQvtCx0YDQsNCx0L7RgtGH0LjQuiDRgdC+0LHRi9GC0LjRjy4g0JLQtdGI0LDQtdGC0YHRjyDQvdCwIG9uY2hhbmdlINCy0L3Rg9GC0YDQuCBFcXVhdGlvbi5fX2NvbnN0cnVjdG9yX18uXG4gKiDQv9GA0L7QstC10YDRj9C10YIg0L/RgNCw0LLQuNC70YzQvdC+INC70Lgg0LLRi9GH0LjRgdC70LXQvSDQv9GA0LjQvNC10YBcbiAqL1xuRXF1YXRpb24ucHJvdG90eXBlLmhhbmRsZXJTb2x2ZSA9IGZ1bmN0aW9uKCl7XG4gICAgaWYodGhpcy50ZXJtc1sxXSA9PSB0aGlzLnNvbHZpbmcudmFsdWUpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCIhIdGA0LXRiNC10L3QvlwiKTtcbiAgICAgICAgdGhpcy50b2dnbGVTb2x2ZSgpO1xuICAgICAgICB0aGlzLnNob3dTb2x2aW5nKCk7XG4gICAgICAgIHRoaXMubGVzc29uLmxvb3AuZW1pdChcImVxdWF0aW9uU29sdmVkXCIpO1xuICAgIH1lbHNle1xuICAgICAgICBjb25zb2xlLmxvZyhcItC+0YLQstC10YIg0L3QtdCy0LXRgNC90YvQuVwiKTtcbiAgICAgICAgZDMuc2VsZWN0KHRoaXMuc29sdmluZykuY2xhc3NlZChcImxzbi13cmFwcGVyX2lucHV0LS1pbnZhbGlkXCIsIHRydWUpO1xuICAgIH1cbn07XG4vKipcbiAqIEBmdW5jdGlvbiDQlNC+0LHQsNCy0LvRj9C10YIg0LIg0YDQtdC30YPQu9GM0YLQsNGCINC30L3QsNGH0LXQvdC40LVcbiAqL1xuRXF1YXRpb24ucHJvdG90eXBlLnNob3dTb2x2aW5nID0gZnVuY3Rpb24oKXtcbiAgICB0aGlzLnNvbHZpbmdUZXh0LmlubmVySFRNTCA9IHRoaXMudGVybXNbMV07XG59O1xuLyoqXG4gKiBAZnVuY3Rpb24g0L/QtdGA0LXQutC70Y7Rh9Cw0LXRgiDQstC40LTQuNC80L7RgdGC0Ywg0YLQtdC60YHRgdGC0L7QstC+0LPQviDQv9C+0LvRjyDQtNC70Y8g0YDQtdC30YPQu9GM0YLQsCDQuCBcbiAqINGB0LDQvNC+0LPQviDRgNC10LfRg9C70YzRgtCw0YLQsFxuICovXG5FcXVhdGlvbi5wcm90b3R5cGUudG9nZ2xlU29sdmUgPSBmdW5jdGlvbigpe1xuICAgIGlmKHRoaXMuc29sdmluZy5jbGFzc05hbWUuc3BsaXQoXCIgXCIpLmluZGV4T2YoXCJoaWRlXCIpID4gLTEpe1xuICAgICAgICBkMy5zZWxlY3QodGhpcy5zb2x2aW5nKS5jbGFzc2VkKFwiaGlkZVwiLCBmYWxzZSk7XG4gICAgICAgIHRoaXMuc29sdmluZ1RleHQuY2xhc3NOYW1lID0gXCJoaWRlXCI7XG4gICAgfWVsc2V7XG4gICAgICAgIGQzLnNlbGVjdCh0aGlzLnNvbHZpbmcpLmNsYXNzZWQoXCJoaWRlXCIsIHRydWUpO1xuICAgICAgICB0aGlzLnNvbHZpbmdUZXh0LmNsYXNzTmFtZSA9IFwiXCI7XG4gICAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBFcXVhdGlvbjsiLCJmdW5jdGlvbiBFdmVudExvb3AoKXtcbiAgICB0aGlzLmV2ZW50cyA9IHt9O1xuICAgIC8ve1xuICAgIC8vICBldmVudE5hbWU6IFtmbiwgZm4sIGZuLCAuLi5dXG4gICAgLy99XG59O1xuRXZlbnRMb29wLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24obmFtZSl7XG4gICAgaWYodGhpcy5ldmVudHNbbmFtZV0gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgIHZhciBmbnMgPSB0aGlzLmV2ZW50c1tuYW1lXTtcbiAgICAgICAgZm9yKHZhciBpPTA7IGk8Zm5zLmxlbmd0aDsgaSsrKXtcbiAgICAgICAgICAgIGZuc1swXSgpO1xuICAgICAgICB9XG4gICAgfVxufTtcbkV2ZW50TG9vcC5wcm90b3R5cGUub24gPSBmdW5jdGlvbihuYW1lLCBjYWxsYmFjayl7XG4gICAgaWYodGhpcy5ldmVudHNbbmFtZV0gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgIHRoaXMuZXZlbnRzW25hbWVdLnB1c2goY2FsbGJhY2spO1xuICAgIH1lbHNle1xuICAgICAgICB0aGlzLmV2ZW50c1tuYW1lXSA9IFtdO1xuICAgICAgICB0aGlzLmV2ZW50c1tuYW1lXS5wdXNoKGNhbGxiYWNrKTtcbiAgICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IEV2ZW50TG9vcDsiLCJ2YXIgRXZlbnRMb29wID0gcmVxdWlyZShcIi4vRXZlbnRMb29wLmpzXCIpO1xudmFyIEVxdWF0aW9uID0gcmVxdWlyZShcIi4vRXF1YXRpb24uanNcIik7XG52YXIgQm93ID0gcmVxdWlyZShcIi4vQm93LmpzXCIpO1xuXG52YXIgb2JzZXJ2ZXIgPSBuZXcgRXZlbnRMb29wKCk7XG5cbi8qKiBcbiAqINCh0L7Qt9C00LDQtdGCINC90L7QstGL0Lkg0LHQu9C+0LosINC60L7RgtC+0YDRi9C5INCx0YPQtNC10YIg0YHQvtC00LXRgNC20LDRgtGMIHN2ZyDQuCDQu9C40L3QtdC50LrRg1xuICogQHBhcmFtIHtOb2RlfSBjb250YWluZXI9ZG9jdW1lbnQuYm9keSDQkdC70L7Quiwg0LIg0LrQvtGC0L7RgNGL0Lkg0LTQvtC70LbQtdC9INCx0YvRgtGMINCy0LvQvtC20LXQvSBcbiAqICAgICAgICAgICAgICAgICAgICAg0LLQtdGB0Ywg0LrQu9Cw0YHRgVxuICogQHBhcmFtIHtBcnJheS48TnVtYmVyPn0gbnVtYmVycyDQnNCw0YHRgdC40LIg0YHQutC70LDQtNGL0LLQsNC10LzRi9GFINGN0LvQtdC80LXQvdGC0L7QslxuICogQHBhcmFtIHtPYmplY3QuPFN0cmluZywgTnVtYmVyPn0gb3B0aW9ucyDQvtC/0YbQuNC4XG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zLmhlaWdodD02NDAg0LLRi9GB0L7RgtCwXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zLndpZHRoPTQ4MCDRiNC40YDQuNC90LBcbiAqIEBwYXJhbSB7Qm9vbGVhbn0g0J3QsNC60LvQsNC00YvQstCw0LXRgiDQv9C+0LLQtdGA0YUg0LvQuNC90LXQudC60Lgg0YDQtdCw0LvRjNC90YPRjiDQvtGB0YxcbiAqIEBjbGFzc1xuICovXG5mdW5jdGlvbiBMZXNzb24oY29udGFpbmVyLCBudW1iZXJzLCBvcHRpb25zLCBkZWJ1Zyl7XG4gICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgICAvL2RlZmF1bHQgdmFsdWVzXG4gICAgaWYoY29udGFpbmVyID09PSB1bmRlZmluZWQpe1xuICAgICAgICBjb250YWluZXIgPSBkb2N1bWVudC5ib2R5O1xuICAgIH1cbiAgICBpZihvcHRpb25zLndpZHRoID09PSB1bmRlZmluZWQpXG4gICAgICAgIHRoaXMub3B0aW9ucy53aWR0aCA9IDY0MDtcbiAgICBpZihvcHRpb25zLmhlaWdodCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICB0aGlzLm9wdGlvbnMuaGVpZ2h0ID0gNDgwO1xuICAgIC8vZW5kIGRlZmF1bHQgdmFsc1xuXG4gICAgLy9ib3dzXG4gICAgdGhpcy5ib3dzID0gW107XG5cbiAgICAvL2NyZWF0ZSBzcGVjaWZpYyB3cmFwcGVyXG4gICAgdmFyIHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuICAgIHdyYXBwZXIuY2xhc3NOYW1lID0gXCJsc24td3JhcHBlclwiO1xuICAgIHdyYXBwZXIuc3R5bGUud2lkdGggPSB0aGlzLm9wdGlvbnMud2lkdGgrXCJweFwiO1xuICAgIHdyYXBwZXIuc3R5bGUuaGVpZ2h0ID0gdGhpcy5vcHRpb25zLmhlaWdodCtcInB4XCI7XG5cbiAgICB2YXIgbWVhc3VyZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG4gICAgbWVhc3VyZS5jbGFzc05hbWUgPSBcImxzbi13cmFwcGVyX21lYXN1cmVcIjtcblxuICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQobWVhc3VyZSk7XG5cbiAgICB0aGlzLndyYXBwZXIgPSBjb250YWluZXIuYXBwZW5kQ2hpbGQod3JhcHBlcik7XG5cbiAgICB0aGlzLmVxdWF0aW9uID0gbmV3IEVxdWF0aW9uKHRoaXMsIFtudW1iZXJzLmpvaW4oXCIrXCIpK1wiPVwiLCBkMy5zdW0obnVtYmVycyldKTtcblxuICAgIC8vY3JlYXRlIHBlcnNvbmFsIGZ1bmN0aW9uc1xuICAgIHZhciBzdGFydFggPSB0aGlzLm9wdGlvbnMud2lkdGgvMTAwKjMuOSxcbiAgICAgICAgc3RhcnRZID0gdGhpcy5vcHRpb25zLndpZHRoLzEwMCo2Ljc7XG4gICAgdGhpcy5zY2FsZVggPSBkMy5zY2FsZS5saW5lYXIoKS5yYW5nZShbc3RhcnRYLCAodGhpcy5vcHRpb25zLndpZHRoLXN0YXJ0WSldKS5kb21haW4oWzAsIDIwXSk7XG4gICAgdGhpcy5zY2FsZVkgPSBkMy5zY2FsZS5saW5lYXIoKS5yYW5nZShbMCwgKHRoaXMub3B0aW9ucy5oZWlnaHQtNjApXSkuZG9tYWluKFsodGhpcy5vcHRpb25zLmhlaWdodC02MCksIDBdKTtcblxuICAgIHZhciBzdmcgPSB0aGlzLnN2ZyA9IGQzLnNlbGVjdCh0aGlzLndyYXBwZXIpLmFwcGVuZCgnc3ZnJylcbiAgICAgICAgLmF0dHIoJ3dpZHRoJywgdGhpcy5vcHRpb25zLndpZHRoKVxuICAgICAgICAuYXR0cignaGVpZ2h0JywgdGhpcy5vcHRpb25zLmhlaWdodCk7XG5cbiAgICAvL2NyZWF0ZSBhcnJvdyBtYXJrZXIgZm9yIGVuZCBvZiBib3dzXG4gICAgc3ZnLmFwcGVuZChcImRlZnNcIikuYXBwZW5kKFwibWFya2VyXCIpXG4gICAgICAgIC5hdHRyKFwiaWRcIiwgXCJhcnJvd2hlYWRcIilcbiAgICAgICAgLmF0dHIoXCJyZWZYXCIsIDQpXG4gICAgICAgIC5hdHRyKFwicmVmWVwiLCAyKVxuICAgICAgICAuYXR0cihcIm1hcmtlcldpZHRoXCIsIDE1KVxuICAgICAgICAuYXR0cihcIm1hcmtlckhlaWdodFwiLCAxNSlcbiAgICAgICAgLmF0dHIoXCJvcmllbnRcIiwgXCJhdXRvXCIpXG4gICAgICAgIC5hcHBlbmQoXCJwYXRoXCIpXG4gICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJhcnJvd1wiKVxuICAgICAgICAuYXR0cihcImRcIiwgdGhpcy5fbGluZShbey8vaGFyZGNvZGVkXG4gICAgICAgICAgICB4OiAwLHk6MFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICB4OiA0LHk6MlxuICAgICAgICB9LHtcbiAgICAgICAgICAgIHg6MCwgeTo0XG4gICAgICAgIH1dKSk7XG5cblxuXG4gICAgaWYoZGVidWcpe1xuICAgICAgICB2YXIgeEF4aXMgPSBkMy5zdmcuYXhpcygpLnNjYWxlKHRoaXMuc2NhbGVYKVxuICAgICAgICAgICAgLm9yaWVudChcImJvdHRvbVwiKS50aWNrcyg0KTtcbiAgICAgICAgc3ZnLmFwcGVuZChcImdcIilcbiAgICAgICAgICAgIC5hdHRyKFwiY2xhc3NcIiwgXCJ4IGF4aXNcIilcbiAgICAgICAgICAgIC5hdHRyKFwidHJhbnNmb3JtXCIsIFwidHJhbnNsYXRlKDAsXCIgKyAodGhpcy5vcHRpb25zLmhlaWdodC01MCkgKyBcIilcIilcbiAgICAgICAgICAgIC5jYWxsKHhBeGlzKTtcbiAgICB9XG59XG4vKipcbiAqIEBmdW5jdGlvbiDQuNGB0L/QvtC70YzQt9GD0LXRgtGB0Y8g0LTQu9GPINGA0LjRgdC+0LLQsNC90LjQtSDRgdGC0YDQtdC70L7Rh9C60Lgt0LzQsNGA0LrQtdGA0LBcbiAqL1xuTGVzc29uLnByb3RvdHlwZS5fbGluZSA9IGQzLnN2Zy5saW5lKClcbiAgICAueChmdW5jdGlvbihkKSB7IHJldHVybiBkLng7IH0pXG4gICAgLnkoZnVuY3Rpb24oZCkgeyByZXR1cm4gZC55OyB9KTtcblxuXG4vKipcbiAqIEBkZXByZWNhdGVkINC40YHQv9C+0LvRjNC30L7QstCw0LvQsNGB0Ywg0LTQu9GPINGB0YLQsNGA0L7QuSDRgNC10LDQu9C40LfQsNGG0LjQuCBhZGRCb3cuIFxuICog0KHQtdC50YfQsNGBINC00YPQsdC70LjRgNGD0LXRgiBCb3cuX2Jvd0xpbmVcbiAqL1xuTGVzc29uLnByb3RvdHlwZS5fYm93TGluZSA9IGQzLnN2Zy5saW5lKCkgLy90aGlzIC0gaW5zdGFuY2UgTGVzc29uXG4gICAgLmludGVycG9sYXRlKFwiYmFzaXNcIilcbiAgICAgICAgLyoqXG4gICAgICAgICogQHRoaXMgTGVzc29uIGluc3RhbmNlXG4gICAgICAgICovXG4gICAgLngoZnVuY3Rpb24oZCkge3JldHVybiB0aGlzLnNjYWxlWChkLngpO30pXG4gICAgLnkoZnVuY3Rpb24oZCkgeyByZXR1cm4gdGhpcy5zY2FsZVkoZC55KTsgfSk7XG5cbi8qKlxuICogQGZ1bmN0aW9uINGB0L7Qt9Cw0LXRgiDQutC70LDRgdGBIEJvdyDQuCDRgNC40YHRg9C10YIg0LTRg9Cz0YNcbiAqIEBwYXJhbSB7TnVtYmVyfSBmcm9tINC+0YLQutGD0LTQsCBbMCwyMF1cbiAqIEBwYXJhbSB7TnVtYmVyfSB0b3Ag0LrRg9C00LAgW2Zyb20sMjBdXG4gKi9cbkxlc3Nvbi5wcm90b3R5cGUuYWRkQm93ID0gZnVuY3Rpb24oZnJvbSwgdG8pe1xuICAgIHZhciBib3cgPSBuZXcgQm93KHRoaXMsIGZyb20sIHRvKTtcbiAgICB0aGlzLmJvd3MucHVzaChib3cpO1xuXG4gICAgYm93Ll9yZW5kZXIoKTtcbn07XG4vKipcbiAqIEBmdW5jdGlvbiDRgdC+0LfQtNCw0LXRgiBvYnNlcnZlciDQuCDQv9C+0LTQv9C40YHRi9Cy0LDQtdGC0YHRjyDQvdCwINGB0L7QsdGL0YLQuNGPINGB0LLRj9C30LDQvdC90YvRhSDRjdC70LXQvNC10L3RgtC+0LJcbiAqINC30LDQv9GD0YHQutCw0LXRgiDRhtC40LrQuyDRgdC+0LHRi9GC0LjQuVxuICovXG5MZXNzb24ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uKCl7XG4gICAgdmFyIG9ic2VydmVyID0gdGhpcy5sb29wID0gbmV3IEV2ZW50TG9vcCgpO1xuICAgIHZhciBpPTA7XG4gICAgdGhpcy5ib3dzW2ldLnNob3dJbnB1dCgpO1xuXG4gICAgdmFyIGluc3RhbmNlID0gdGhpcztcbiAgICBvYnNlcnZlci5vbihcImJvd1NvbHZlZFwiLCBmdW5jdGlvbigpe1xuICAgICAgICBpZihpbnN0YW5jZS5ib3dzWysraV0gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICBpbnN0YW5jZS5ib3dzW2ldLnNob3dJbnB1dCgpO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIGluc3RhbmNlLmVxdWF0aW9uLnRvZ2dsZVNvbHZlKCk7XG4gICAgICAgICAgICBvYnNlcnZlci5vbihcImVxdWF0aW9uU29sdmVkXCIsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZW1pdChcImxlc3NvbkNvbXBsZXRlXCIpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9KTtcblxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBMZXNzb247IiwidmFyIExlc3NvbiA9IHJlcXVpcmUoXCIuL0xlc3Nvbi5qc1wiKTtcblxuXG52YXIgbGVzc29uID0gbmV3IExlc3Nvbihkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImR5bmFtaWNcIiksIFs1LDZdLCB7XG4gICAgaGVpZ2h0OiAzMDAsXG4gICAgd2lkdGg6IDgwMFxufSk7XG5sZXNzb24uYWRkQm93KDAsIDUpO1xubGVzc29uLmFkZEJvdyg1LCAxMSk7XG5sZXNzb24ucnVuKCk7XG5cbmxlc3Nvbi5sb29wLm9uKFwibGVzc29uQ29tcGxldGVcIiwgZnVuY3Rpb24oKXtcbiAgICBhbGVydChcImxlc3NvbiBjb21wbGV0ZVwiKTtcbn0pO1xuXG4iXX0=
